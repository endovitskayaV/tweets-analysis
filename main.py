import numpy
import pandas
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression

from analyser import find_top_n
from const import CLASS_COLUMN_NAME, TEXT_COLUMN_NAME, PREPROCESSED_FILENAME, INITIAL_DATA_FILENAME
from construct import construct_df
from fit import fit, fit_gaussian
from preprocess import clean_data
from test import test
from train import find_best_params
from utils import write_to_file, get_split


def evaluate_raw_data_results(df):
    df = df.reindex(numpy.random.permutation(df.index))
    X_train, y_train, X_test, y_test = get_split(df)

    vectorizer = TfidfVectorizer()
    X_train = vectorizer.fit_transform(X_train)

    logreg = LogisticRegression(max_iter=1000)
    logreg.fit(X_train, y_train)

    X_test = vectorizer.transform(X_test)
    write_to_file('raw_log_reg.txt', logreg.score(X_test, y_test))


def to_array(array_string):
    return array_string.replace('[', '').replace(']', '').replace('\'', '').split(', ')


def main():
    construct_df()
    df = pandas.read_csv(INITIAL_DATA_FILENAME, sep=';', header=None, names=['id', TEXT_COLUMN_NAME, CLASS_COLUMN_NAME])
    evaluate_raw_data_results(df)
    clean_data(df)
    df = pandas.read_csv(PREPROCESSED_FILENAME, sep=';', header=None, names=['id', TEXT_COLUMN_NAME, CLASS_COLUMN_NAME],
                         converters={TEXT_COLUMN_NAME: to_array})
    df = df[[TEXT_COLUMN_NAME, CLASS_COLUMN_NAME]]
    find_top_n(df)
    find_best_params(df)
    X_test, y_test, model = fit(df)

    test(X_test, y_test, model, 'logreg')

    X_test, y_test, model = fit_gaussian(df)

    test(X_test, y_test, model, 'gauss')


if __name__ == '__main__':
    main()
