import numpy
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV

from const import BEST_SCORE_FILENAME, BEST_C_FILENAME
from utils import identity, write_to_file, get_split


def find_best_params(df):
    df = df.reindex(numpy.random.permutation(df.index))
    X_train, y_train, X_test, y_test = get_split(df)

    vectorizer = TfidfVectorizer(tokenizer=identity, lowercase=False)
    X_train = vectorizer.fit_transform(X_train)

    grid_search = GridSearchCV(LogisticRegression(max_iter=1000),
                               cv=5,
                               param_grid={'C': [1e-5, 1e-3, 1e-1, 1e0, 1e1, 1e2]},
                               n_jobs=-1)
    grid_search.fit(X_train, y_train)
    write_to_file(BEST_SCORE_FILENAME, grid_search.best_score_)
    write_to_file(BEST_C_FILENAME, grid_search.best_params_['C'])
