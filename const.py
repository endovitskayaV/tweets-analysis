INITIAL_DATA_FILENAME = 'initial.csv'
PREPROCESSED_FILENAME = 'preprocessed_no_stem.csv'
BEST_C_FILENAME = 'best_c.txt'
BEST_SCORE_FILENAME = 'best_score.txt'

TEXT_COLUMN_NAME = 'ttext'
CLASS_COLUMN_NAME = 'class'
