from sklearn.model_selection import train_test_split

from const import CLASS_COLUMN_NAME, TEXT_COLUMN_NAME


def write_to_file(filename, content):
    with open(filename, 'w') as file:
        file.write(str(content))


def identity(w):
    return w


def get_split(df):
    X_train, X_test = train_test_split(df, test_size=0.3)
    y_train = X_train[CLASS_COLUMN_NAME]
    y_test = X_test[CLASS_COLUMN_NAME]
    X_train = X_train[TEXT_COLUMN_NAME]
    X_test = X_test[TEXT_COLUMN_NAME]
    return X_train, y_train, X_test, y_test
