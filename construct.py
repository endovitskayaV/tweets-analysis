import pandas

from const import TEXT_COLUMN_NAME, CLASS_COLUMN_NAME, INITIAL_DATA_FILENAME


def construct_df():
    column_names = ['id', 'tdate', 'tmane', TEXT_COLUMN_NAME, 'ttype', 'trep', 'tfav', 'tstcount', 'tfol', 'tfrien',
                    'listcount', 'unknown']
    negative_tweets_df = pandas.read_csv("negative.csv", sep=';', header=None, names=column_names)
    positive_tweets_df = pandas.read_csv("positive.csv", sep=';', header=None, names=column_names)
    df = pandas.concat([negative_tweets_df, positive_tweets_df]).reset_index(drop=True)
    df[CLASS_COLUMN_NAME] = [0] * len(negative_tweets_df.index) + [1] * len(positive_tweets_df.index)
    df = df[[TEXT_COLUMN_NAME, CLASS_COLUMN_NAME]]
    df.to_csv(INITIAL_DATA_FILENAME, sep=';', header=False)
