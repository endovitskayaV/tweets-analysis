import re

from nltk import SnowballStemmer, WordPunctTokenizer
from nltk.corpus import stopwords
from pymystem3 import Mystem

from const import TEXT_COLUMN_NAME, PREPROCESSED_FILENAME

STOP_WORDS = set(stopwords.words("russian"))

stemmer = SnowballStemmer('russian')


def lemmize(word):
    return Mystem().lemmatize(word)[0]


def filter_word(word):
    return word not in STOP_WORDS


def clean_text(text):
    text = text.lower()
    text = re.sub(r'@\S+', '', text)  # mentions
    text = re.sub(r'http\S+', '', text)  # links
    text = re.sub(r'(&[lg]t;)', '', text)  # &lt; &gt;
    text = re.sub(r'(rt)', '', text)
    text = re.sub(r':-?[^)(]', '', text)  # smiles except :-) :-(
    text = re.sub(r'[0-9~`!?:,.;"*/|\'#$%&_—+=\-><\\}{\]\[»«”“]+', '', text)
    text = re.sub(r'\(+', '(', text)
    text = re.sub(r'\)+', ')', text)
    text = text.strip()
    text = WordPunctTokenizer().tokenize(text)
    text = [word for word in text if filter_word(word)]
    return text


def clean_data(df):
    df[TEXT_COLUMN_NAME] = list(map(clean_text, df[TEXT_COLUMN_NAME]))
    df.to_csv(PREPROCESSED_FILENAME, sep=';', header=False)
