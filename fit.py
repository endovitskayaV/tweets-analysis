import numpy
import pandas
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB

from const import BEST_C_FILENAME
from utils import identity, get_split


def fit(df):
    df = df.reindex(numpy.random.permutation(df.index))
    X_train, y_train, X_test, y_test = get_split(df)

    vectorizer = TfidfVectorizer(tokenizer=identity, lowercase=False)
    X_train = vectorizer.fit_transform(X_train)

    with open(BEST_C_FILENAME) as f:
        best_C = float(f.read())

    model = LogisticRegression(max_iter=1000, C=best_C)
    model.fit(X_train, y_train)

    X_test = vectorizer.transform(X_test)

    return X_test, y_test, model

def fit_gaussian(df):
    df = df.reindex(numpy.random.permutation(df.index))
    X_train, y_train, X_test, y_test = get_split(df)

    vectorizer = TfidfVectorizer(tokenizer=identity, lowercase=False, max_features=10000)
    X_train = vectorizer.fit_transform(X_train).toarray()

    model = GaussianNB()
    model.fit(X_train, y_train)

    X_test = vectorizer.transform(X_test).toarray()

    return X_test, y_test,model

