from sklearn.feature_extraction.text import TfidfVectorizer

from const import TEXT_COLUMN_NAME, CLASS_COLUMN_NAME
from utils import write_to_file, identity


def do_find_top_n(text):
    vectorizer = TfidfVectorizer(tokenizer=identity, analyzer='word', lowercase=False, max_features=10)
    vectorizer.fit_transform(text)
    return vectorizer.get_feature_names()


def find_top_n(df):
    write_to_file('top_neg.txt', do_find_top_n(df[df[CLASS_COLUMN_NAME] == 0][TEXT_COLUMN_NAME]))
    write_to_file('top_pos.txt', do_find_top_n(df[df[CLASS_COLUMN_NAME] == 1][TEXT_COLUMN_NAME]))
