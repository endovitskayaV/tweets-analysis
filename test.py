from matplotlib import pyplot as plt
from sklearn.metrics import f1_score, roc_auc_score, roc_curve, classification_report

from utils import write_to_file


def test(X_test, y_test, model, prefix):
    y_pred = model.predict(X_test)
    write_to_file(prefix + '_accuracy.txt', model.score(X_test, y_test))
    write_to_file(prefix + '_f1.txt', f1_score(y_test, y_pred))
    write_to_file(prefix + '_classification_report.txt', classification_report(y_test, y_pred))

    probs = model.predict_proba(X_test)[:, 1]
    area = roc_auc_score(y_test, probs)
    fpr, tpr, thresholds = roc_curve(y_test, probs)
    plt.plot(fpr, tpr, color='darkorange', label=prefix + ' (area = %s)' % area)
    plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC curve')
    plt.legend(loc="lower right")
    plt.savefig(prefix+'_roc_auc.png')
